import { Builder } from "/loader/builder.js"

export class EditorBuild {
  deps = [
    'forms/button-group.js',
    'dialog/dialog.js',
    'menu/dropdown.js',
    'editor/file-group.js',
    'editor/file-view.js',
    'editor/text-edit.js',
    'loader/builder.js',
    'editor/app.js',
  ]

  constructor() {
    this.files = undefined
  }

  async loadFiles() {
    const files = []
    for (const name of this.deps) {
      const resp = await fetch(name)
      files.push({
        name,
        data: await resp.text(),
      })
    }
    this.files = files
  }

  async build() {
    if (this.files === undefined) {
      await this.loadFiles()
    }
    const builder = new Builder(this.files)
    return builder.build()
  }
}